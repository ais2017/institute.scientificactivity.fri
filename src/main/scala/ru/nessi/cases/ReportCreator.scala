package ru.nessi.cases

import ru.nessi.business.{Document, Group, ObjectBuilder}
import ru.nessi.storage.Storage

class ReportCreator(storage: Storage, calc: EffectivenessCalc) {

  private[this] val objectBuilder = new ObjectBuilder(storage)

  private[cases] def createReport(): String = {
    val builder = new StringBuilder
    builder.append(s"University has ${storage.getAllGroups.size} scientific groups.\n")
    storage.getAllGroups.map(objectBuilder.buildGroup).foreach(group => {
      builder.append(s"Group ${group.name} consists of ${group.getPersons.size} persons: \n")
      group.getPersons.foreach(person => {
        builder.append(s"${person.name}\n")
      })
      builder.append(s"It has total of ${getAllDocumentsOfThisGroup(group).size} publications.\n" +
        s"It's effectiveness is ${calc.calcGroupEffectiveness(group)}\n\n")
    })
    builder.append(s"Effectiveness of university is ${calc.calcTotalEffectiveness()}\n" + "-"*20 + "\n")
    builder.toString()
  }

  private[this] def getAllDocumentsOfThisGroup(group: Group): collection.mutable.ArrayBuffer[Document] = {
    storage.getAllDocuments.map(objectBuilder.buildDocument).filter(doc => doc.getGroups.contains(group))
  }
}
