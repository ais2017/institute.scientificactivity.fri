package ru.nessi.cases

import ru.nessi.business.{ObjectBuilder, User}
import ru.nessi.storage.Storage

class UserCreator(storage: Storage) {

  private[this] val objectBuilder = new ObjectBuilder(storage)

  private[cases] def createUser(user: User): Long = {
    storage.getAllUsers.map(objectBuilder.buildUser).find(_.login == user.login) match {
      case Some(_) => throw new RuntimeException("User with such login already exists")
      case _ => storage.addUser(user)
    }
  }

}
