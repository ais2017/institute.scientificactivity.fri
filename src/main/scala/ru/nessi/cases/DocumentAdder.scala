package ru.nessi.cases

import ru.nessi.business.{Document, ObjectBuilder}
import ru.nessi.storage.Storage

class DocumentAdder(private val storage: Storage, documentValidator: DocumentValidator) {

  private[this] val objectBuilder = new ObjectBuilder(storage)

  private[cases] def addDocument(doc: Document): Long = {
    if(!documentValidator.validate(doc)) throw new RuntimeException("Document is not valid!")
    if(storage.getAllDocuments.nonEmpty && storage.getAllDocuments.map(it => objectBuilder.buildDocument(it)).contains(doc)) throw new RuntimeException("Document already exists!")
    storage.addDocument(doc)
  }

}
