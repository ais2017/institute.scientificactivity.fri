package ru.nessi.cases

import ru.nessi.business.Document

trait DocumentValidator {

  private[cases] def validate(document: Document): Boolean

}