package ru.nessi.cases

import ru.nessi.business.Group

trait EffectivenessCalc {

  private[cases] def calcGroupEffectiveness(group: Group): Double

  def calcTotalEffectiveness(): Double
}
