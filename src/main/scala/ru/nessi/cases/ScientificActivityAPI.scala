package ru.nessi.cases

import java.io.File

import ru.nessi.business._
import ru.nessi.storage.Storage

class ScientificActivityAPI(storage: Storage,
                            documentValidator: DocumentValidator,
                            effectivenessCalc: EffectivenessCalc) {

  private[this] val documentAdder = new DocumentAdder(storage, documentValidator)
  private[this] val reportCreator = new ReportCreator(storage, effectivenessCalc)
  private[this] val userCreator = new UserCreator(storage)
  private[this] val objectBuilder = new ObjectBuilder(storage)

  def addDocument(name: String, authorID: Long, file: File = null, externalID: String = null): Long = {
    val author = objectBuilder.fillPerson(authorID, objectBuilder.buildPerson(authorID), new collection.mutable.ArrayBuffer[(String, Long)]())
    val doc  = new Document(name, author, file, externalID)
    documentAdder.addDocument(doc)
  }

  def getDocumentForUser(docId: Long, userId: Long): Document = {
    val doc = objectBuilder.buildDocument(docId)
    if(objectBuilder.buildUser(userId).rights.containRightsOnDocument(doc)) doc
    else throw new RuntimeException("Have no access to this document")
  }

  def createReport(): String = reportCreator.createReport()

  def createUser(name: String, accessLevel: String, login: String, pass: String, creatorId: Long): Long = {
    val creator = objectBuilder.buildUser(creatorId)
    if (creator.accessLevel != "ADMIN") throw new RuntimeException("You don't have permission to do that")
    userCreator.createUser(new User(name, accessLevel, new Rights, login, pass))
  }
}
