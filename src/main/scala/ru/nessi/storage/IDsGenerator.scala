package ru.nessi.storage

trait IDsGenerator {

  def generateDocumentID: Long

  def generateGroupID: Long

  def generateDocumentGroupID: Long

  def generatePersonID: Long

  def generateUserID: Long

  def generateRightsID: Long

}
