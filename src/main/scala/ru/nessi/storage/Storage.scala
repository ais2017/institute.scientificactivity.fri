package ru.nessi.storage

import java.io.File

import ru.nessi.business._

trait Storage{

  val iDsGenerator: IDsGenerator

  //get all
  def getAllGroups: collection.mutable.ArrayBuffer[Long]
  def getAllDocuments: collection.mutable.ArrayBuffer[Long]
  def getAllUsers: collection.mutable.ArrayBuffer[Long]
  def getAllPersons: collection.mutable.ArrayBuffer[Long]
  def getAllRights: collection.mutable.ArrayBuffer[Long]
  def getAllDocumentGroups: collection.mutable.ArrayBuffer[Long]

  //add
  def addDocument(doc: Document): Long
  def addUser(user: User): Long
  def addPerson(person: Person): Long
  def addGroup(group: Group): Long
  def addRights(rights: Rights): Long
  def addDocumentGroup(documentGroup: DocumentGroup): Long

  //get one
  def getPersonByID(id: Long): Option[(String, collection.mutable.ArrayBuffer[Long])]
  def getGroupByID(id: Long): Option[(String, Long, collection.mutable.ArrayBuffer[Long])]
  def getDocumentByID(id: Long): Option[(String, Long, collection.mutable.ArrayBuffer[Long], File, String)]
  def getUserByID(id: Long): Option[(String, String, Long, String, String)]
  def getDocumentGroupByID(id: Long): Option[(String, collection.mutable.ArrayBuffer[Long])]
  def getRightsByID(id: Long): Option[(collection.mutable.ArrayBuffer[Long], collection.mutable.ArrayBuffer[Long])]
}
