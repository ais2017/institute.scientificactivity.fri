package ru.nessi.business

class Group(val name: String) {

  private var leader: Person = _
  val persons: collection.mutable.ArrayBuffer[Person] = new collection.mutable.ArrayBuffer[Person]()

  def addPerson(person: Person): Unit = {
    if (!this.persons.contains(person)) {
      persons.append(person)
      person.addGroup(this)
    }
  }

  def removePerson(person: Person): Unit = {
    if (this.persons.contains(person)) {
      persons -= person
      person.removeGroup(this)
    }
  }

  def getPersons: collection.mutable.ArrayBuffer[Person] = persons

  def setLeader(person: Person): Unit = person match {
    case null => throw new RuntimeException("Leader can't be assigned to null")
    case newLeader => leader = newLeader
  }

  def getLeader = leader


  def canEqual(other: Any): Boolean = other.isInstanceOf[Group]

  override def equals(other: Any): Boolean = other match {
    case that: Group =>
      (that canEqual this) &&
        leader == that.leader &&
        persons == that.persons &&
        name == that.name
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(if (leader == null) 0 else leader, persons, name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
