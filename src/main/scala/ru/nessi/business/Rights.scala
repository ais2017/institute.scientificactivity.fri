package ru.nessi.business

import scala.collection.mutable

class Rights {
  private val documents: collection.mutable.ArrayBuffer[Document] = new mutable.ArrayBuffer[Document]()
  private val documentGroups: collection.mutable.ArrayBuffer[DocumentGroup] = new collection.mutable.ArrayBuffer[DocumentGroup]()

  def addDocument(document: Document): Unit = {
    documents.append(document)
  }

  def addDocumentGroup(documentGroup: DocumentGroup): Unit = {
    documentGroups.append(documentGroup)
  }

  def removeDocument(document: Document): Unit = {
    documents -= document
  }

  def removeDocumentGroup(documentGroup: DocumentGroup): Unit = {
    documentGroups -= documentGroup
  }

  def getDocuments: collection.mutable.ArrayBuffer[Document] = documents

  def getDocumentGroups: collection.mutable.ArrayBuffer[DocumentGroup] = documentGroups

  def containRightsOnDocument(document: Document): Boolean = documents.contains(document) || documentGroups.flatMap(_.getDocuments).contains(document)

  def containRightsOnDocumentGroup(documentGroup: DocumentGroup): Boolean = documentGroups.contains(documentGroup)


  def canEqual(other: Any): Boolean = other.isInstanceOf[Rights]

  override def equals(other: Any): Boolean = other match {
    case that: Rights =>
      (that canEqual this) &&
        documents == that.documents &&
        documentGroups == that.documentGroups
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(documents, documentGroups)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}