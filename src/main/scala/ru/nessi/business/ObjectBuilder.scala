package ru.nessi.business

import ru.nessi.storage.Storage

class ObjectBuilder(storage: Storage) {

  def fillPerson(id: Long, obj: Person, used: collection.mutable.ArrayBuffer[(String, Long)]): Person = {
    storage.getPersonByID(id) match {
      case None => throw new RuntimeException(s"No person with id $id")
      case Some((_, groupsID)) =>
        used.append(("person", id))
        val groups = groupsID.map(groupID => (groupID, buildGroup(groupID)))
        val filtered = groups.filter(pair => used.filter(usedPair => usedPair._1 == "group").map(_._2).contains(pair._1))
        val resultedGroups =  filtered.map(pair => fillGroup(pair._1, pair._2, used))
        resultedGroups.foreach(obj.addGroup)
        obj
    }
  }
  def fillGroup(id: Long, group: Group, used: collection.mutable.ArrayBuffer[(String, Long)]): Group = {
    storage.getGroupByID(id) match {
      case None => throw new RuntimeException(s"No group with id $id")
      case Some((_, leaderId, persons)) =>
        used.append(("group", id))
        group.setLeader(fillPerson(leaderId, buildPerson(leaderId), used))
        val personPairs = persons.map(personID => (personID, buildPerson(personID)))
        val filtered = personPairs.filter(pair => used.filter(usedPair => usedPair._1 == "person").map(_._2).contains(pair._1))
        val resultedPersons = filtered.map(pair => fillPerson(pair._1, pair._2, used))
        resultedPersons.foreach(group.addPerson)
        group
    }
  }


  def buildPerson(id: Long): Person = storage.getPersonByID(id) match {
    case Some((name, _)) => new Person(name)
    case _ => throw new RuntimeException(s"No person with id $id")
  }

  def buildDocument(id: Long): Document = storage.getDocumentByID(id) match {
    case Some((name, personID, _, file, externalID)) =>
      val used: collection.mutable.ArrayBuffer[(String, Long)] = new collection.mutable.ArrayBuffer()
      val person = fillPerson(personID, buildPerson(personID), used)
      if(file == null && externalID == null) {
        new Document(name, person)
      } else if(file == null)
        new Document(name, person, externalID)
      else
        new Document(name, person, file)
    case _ => throw new RuntimeException(s"No document with id $id")
  }

  def buildDocumentGroup(id: Long): DocumentGroup = storage.getDocumentGroupByID(id) match {
    case Some((name, groupIDs)) =>
      val documentGroup = new DocumentGroup(name)
      groupIDs.foreach(docID => documentGroup.addDocument(buildDocument(docID)))
      documentGroup
    case _ => throw new RuntimeException(s"No document group with id $id")
  }

  def buildGroup(id: Long): Group = storage.getGroupByID(id) match {
    case Some((name, _, _)) => new Group(name)
    case _ => throw new RuntimeException(s"No group with id $id")
  }

  def buildRights(id: Long): Rights = storage.getRightsByID(id) match {
    case Some((documentsIDs, documentGroupsIDs)) =>
      val result = new Rights
      documentGroupsIDs.foreach(dgID => result.addDocumentGroup(buildDocumentGroup(dgID)))
      documentsIDs.foreach(dID => result.addDocument(buildDocument(dID)))
      result
    case _ => throw new RuntimeException(s"No rights with id $id")
  }

  def buildUser(id: Long): User = storage.getUserByID(id) match {
    case Some((name, accessLevel, rightsID, login, pass)) => new User(name, accessLevel, buildRights(rightsID), login, pass)
    case _ => throw new RuntimeException(s"No user with id $id")
  }
}