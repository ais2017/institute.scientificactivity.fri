package ru.nessi.business

class User(val name: String,
           val accessLevel: String,
           val rights: Rights,
           val login: String,
           val pass: String) {

  def canEqual(other: Any): Boolean = other.isInstanceOf[User]

  override def equals(other: Any): Boolean = other match {
    case that: User =>
      (that canEqual this) &&
        name == that.name &&
        accessLevel == that.accessLevel &&
        rights == that.rights &&
        login == that.login &&
        pass == that.pass
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(name, accessLevel, rights, login, pass)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}