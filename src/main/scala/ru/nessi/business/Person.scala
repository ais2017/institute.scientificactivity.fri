package ru.nessi.business

class Person(val name: String) {

  private val groups: collection.mutable.ArrayBuffer[Group] = new collection.mutable.ArrayBuffer[Group]()

  def addGroup(group: Group): Unit = {
    if (!this.groups.contains(group)) {
      groups.append(group)
      group.addPerson(this)
    }
  }

  def removeGroup(group: Group): Unit = {
    if (this.groups.contains(group)) {
      groups -= group
      group.removePerson(this)
    }
  }

  def getGroups: collection.mutable.ArrayBuffer[Group] = groups

  def canEqual(other: Any): Boolean = other.isInstanceOf[Person]

  override def equals(other: Any): Boolean = other match {
    case that: Person =>
      (that canEqual this) &&
        groups == that.groups &&
        name == that.name
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(groups, name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}