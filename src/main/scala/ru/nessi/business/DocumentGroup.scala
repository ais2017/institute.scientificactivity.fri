package ru.nessi.business

class DocumentGroup(val name: String) {

  private val documents: collection.mutable.ArrayBuffer[Document] = new collection.mutable.ArrayBuffer[Document]()

  def addDocument(document: Document): Unit = {
    documents.append(document)
  }

  def removeDocument(document: Document): Unit = {
    documents -= document
  }

  def getDocuments: collection.mutable.ArrayBuffer[Document] = documents

  def canEqual(other: Any): Boolean = other.isInstanceOf[DocumentGroup]

  override def equals(other: Any): Boolean = other match {
    case that: DocumentGroup =>
      (that canEqual this) &&
        documents == that.documents &&
        name == that.name
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(documents, name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}