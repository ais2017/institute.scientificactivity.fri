package ru.nessi.business

import java.io.File

class Document(val name: String, val author: Person, var file: File = null, var externalID: String = null) {

  def this(name: String, author: Person, externalID: String) {
    this(name, author, null, externalID)
  }

  private val groups: collection.mutable.ArrayBuffer[Group] = new collection.mutable.ArrayBuffer[Group]()

  def getGroups: collection.mutable.ArrayBuffer[Group] = groups


  def canEqual(other: Any): Boolean = other.isInstanceOf[Document]

  override def equals(other: Any): Boolean = other match {
    case that: Document =>
      (that canEqual this) &&
        groups == that.groups &&
        name == that.name &&
        author == that.author &&
        file == that.file &&
        externalID == that.externalID
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(groups, name, author, if(file == null) 0 else file, if(externalID == null) 0 else externalID)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}