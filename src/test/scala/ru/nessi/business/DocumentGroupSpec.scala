package ru.nessi.business

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.utils.SpecHelper

class DocumentGroupSpec extends FlatSpecLike with Matchers with SpecHelper with BeforeAndAfterAll{

  it should "create document group with given name" in {
    val dg = new DocumentGroup("name")
    dg.name shouldEqual "name"
  }

  it should "add and remove document to group" in {
    val documentGroup = new DocumentGroup("name")
    documentGroup.addDocument(document)
    documentGroup.getDocuments.contains(document) shouldEqual true
    documentGroup.removeDocument(document)
    documentGroup.getDocuments.size shouldEqual 0
  }

}
