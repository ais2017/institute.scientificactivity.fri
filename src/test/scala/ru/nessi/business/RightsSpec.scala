package ru.nessi.business

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.utils.SpecHelper

class RightsSpec extends FlatSpecLike with Matchers with SpecHelper with BeforeAndAfterAll{

  it should "create empty rights object with correct id" in {
    val rights = new Rights
    rights.getDocumentGroups.isEmpty shouldEqual true
    rights.getDocuments.isEmpty shouldEqual true
  }

  it should "add and remove rights on documents" in {
    val rights = new Rights
    rights.addDocument(document)
    rights.containRightsOnDocument(document) shouldEqual true
    rights.removeDocument(document)
    rights.containRightsOnDocument(document) shouldEqual false
  }

  it should "add and remove rights on document group" in {
    val rights = new Rights
    rights.addDocumentGroup(documentGroup)
    rights.containRightsOnDocumentGroup(documentGroup) shouldEqual true
    rights.removeDocumentGroup(documentGroup)
    rights.containRightsOnDocumentGroup(documentGroup)
  }

}
