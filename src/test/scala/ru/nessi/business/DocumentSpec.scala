package ru.nessi.business

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.utils.SpecHelper

class DocumentSpec extends FlatSpecLike with Matchers with SpecHelper with BeforeAndAfterAll{

  it should "create document with given name and author" in {
    val name = "name"
    val doc = new Document(name, author)
    doc.name shouldEqual name
    doc.author shouldEqual author
  }

  it should "create document with given name, author and id or file" in {
    val name = "name"
    val doc1 = new Document(name, author, "id")
    val doc2 = new Document(name, author, documentFile)
    doc1.name shouldEqual name
    doc1.author shouldEqual author
    doc2.file shouldEqual documentFile
  }



}
