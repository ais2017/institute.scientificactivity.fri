package ru.nessi.business

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.utils.SpecHelper

class GroupSpec extends FlatSpecLike with Matchers with SpecHelper with BeforeAndAfterAll{

  it should "create group with given name" in {
    val group = new Group("group")
    group.name shouldEqual "group"
  }

  it should "set leader to the group" in {
    group.setLeader(leader)
    group.getLeader shouldEqual leader
  }

  it should "add and remove persons to group" in {
    val group = new Group("group")
    group.addPerson(person)
    group.getPersons.contains(person) shouldEqual true
    group.removePerson(person)
    group.getPersons.size shouldEqual 0
  }

}
