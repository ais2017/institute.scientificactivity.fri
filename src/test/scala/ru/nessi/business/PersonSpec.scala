package ru.nessi.business

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.utils.SpecHelper

class PersonSpec extends FlatSpecLike with Matchers with SpecHelper with BeforeAndAfterAll{

  it should "create person with given name and right id" in {
    val person = new Person("person")
    person.name shouldEqual "person"
  }

  it should "add and remove group" in {
    val person = new Person("person")
    person.addGroup(group)
    person.getGroups.contains(group) shouldEqual true
    person.removeGroup(group)
    person.getGroups.size shouldEqual 0
  }

}
