package ru.nessi.cases

import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import ru.nessi.business.{Rights, User}
import ru.nessi.utils.StorageMocker

class UserCreatorSpec extends FlatSpecLike with Matchers with BeforeAndAfterAll{


  val storage = new StorageMocker
  val userCreator = new UserCreator(storage)

  it should "create user" in {
    val oldSize = storage.getAllUsers.size
    val user = new User("user", "USER", new Rights, "login", "pass")
    userCreator.createUser(user)
    storage.getAllUsers.size - oldSize shouldEqual 1
  }

  it should "throw exception because such login already exists" in {
    the[RuntimeException] thrownBy {
      userCreator.createUser(new User("user", "USER", new Rights, "login", "pass"))
    } should have message "User with such login already exists"
  }

}
