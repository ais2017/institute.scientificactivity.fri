package ru.nessi.cases

import org.scalatest.{FlatSpecLike, Matchers}
import ru.nessi.business.Document
import ru.nessi.utils.{FalseDocumentValidator, SpecHelper, StorageMocker, TrueDocumentValidator}

class DocumentAdderSpec extends FlatSpecLike with Matchers with SpecHelper{

  val storage = new StorageMocker
  val validatorTrue = new TrueDocumentValidator
  val validatorFalse = new FalseDocumentValidator
  val doc = new Document("doc", person)

  it should "correctly add document" in {
    val adder = new DocumentAdder(storage, validatorTrue)
    val oldSize = storage.getAllDocuments.size
    adder.addDocument(doc)
    storage.getAllDocuments.size - oldSize shouldEqual 1
  }

  it should "throw exception on adding contained document" in {
    the [RuntimeException] thrownBy {
      new DocumentAdder(storage, validatorTrue).addDocument(doc)
    } should have message "Document already exists!"
  }

  it should "throw exception on adding not valid document" in {
    the [RuntimeException] thrownBy {
      new DocumentAdder(storage, validatorFalse).addDocument(document)
    } should have message "Document is not valid!"
  }

}
