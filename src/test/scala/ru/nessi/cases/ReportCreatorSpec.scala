package ru.nessi.cases

import org.scalatest.{FlatSpecLike, Matchers}
import ru.nessi.utils.{DumbEffectivenessCalc, StorageMocker}

class ReportCreatorSpec extends FlatSpecLike with Matchers{

  it should "create report" in {
    val report = "University has 0 scientific groups.\n" +
      "Effectiveness of university is 0.0\n" +
      "-"*20 + "\n"
    val storage = new StorageMocker
    new ReportCreator(storage, new DumbEffectivenessCalc(storage)).createReport() shouldEqual report
  }

}
