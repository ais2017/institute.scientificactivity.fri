package ru.nessi.cases

import org.scalatest.{FlatSpecLike, Matchers}
import ru.nessi.business.{Rights, User}
import ru.nessi.utils.{DumbEffectivenessCalc, StorageMocker, TrueDocumentValidator}

class APISpec extends FlatSpecLike with Matchers {

  val storage = new StorageMocker
  val admin = new User("admin", "ADMIN", new Rights, "admin", "pass")
  val adminID: Long = storage.addUser(admin)
  val user = new User("user", "USER", new Rights, "uniqueLogin", "pass")
  val userID: Long = storage.addUser(user)
  storage.addUser(user)
  storage.addUser(admin)

  val api = new ScientificActivityAPI(storage, new TrueDocumentValidator, new DumbEffectivenessCalc(storage))

  it should "create new user" in {
    val oldSize = storage.getAllUsers.size
    api.createUser("user", "USER", "login1", "pass", adminID)
    storage.getAllUsers.size - oldSize shouldEqual 1
  }

  it should "throw exception because of rights restriction" in {

    the[RuntimeException] thrownBy {
      api.createUser("user", "USER", "login", "login", userID)
    } should have message "You don't have permission to do that"
  }

}
