package ru.nessi.utils

import ru.nessi.business.{Group, ObjectBuilder}
import ru.nessi.cases.EffectivenessCalc
import ru.nessi.storage.Storage

class DumbEffectivenessCalc(storage: Storage) extends EffectivenessCalc{

  val objectBuilder = new ObjectBuilder(storage)

  override def calcGroupEffectiveness(group: Group): Double = {
    storage.getAllDocuments.count(it => objectBuilder.buildDocument(it).getGroups.contains(group)) match {
      case 0 => 0
      case _ => 1
    }
  }

  override def calcTotalEffectiveness(): Double = {
    var sum: Double = 0
    val groups = storage.getAllGroups.map(it => objectBuilder.fillGroup(it, objectBuilder.buildGroup(it), new collection.mutable.ArrayBuffer[(String, Long)]()))
    for(group <- groups){
      sum += calcGroupEffectiveness(group)
    }
    if(groups.nonEmpty) sum/groups.size else 0
  }

}
