package ru.nessi.utils

import java.io.File

import ru.nessi.business._
import ru.nessi.storage.{IDsGenerator, Storage}

class StorageMocker extends Storage with SpecHelper{
  override val iDsGenerator: IDsGenerator = new DumbIDsGenerator

  private val groups: collection.mutable.ArrayBuffer[(Long, Group)] = new collection.mutable.ArrayBuffer[(Long, Group)]()
  private val documents: collection.mutable.ArrayBuffer[(Long, Document)] = new collection.mutable.ArrayBuffer[(Long, Document)]()
  private val users: collection.mutable.ArrayBuffer[(Long, User)] = new collection.mutable.ArrayBuffer[(Long, User)]()
  private val documentGroups: collection.mutable.ArrayBuffer[(Long, DocumentGroup)] = new collection.mutable.ArrayBuffer[(Long, DocumentGroup)]()
  private val persons: collection.mutable.ArrayBuffer[(Long, Person)] = new collection.mutable.ArrayBuffer[(Long, Person)]()
  private val rights: collection.mutable.ArrayBuffer[(Long, Rights)] = new collection.mutable.ArrayBuffer[(Long, Rights)]()


  //get all
  override def getAllGroups: collection.mutable.ArrayBuffer[Long] = groups.map(_._1)

  override def getAllDocuments: collection.mutable.ArrayBuffer[Long] = documents.map(_._1)

  override def getAllUsers: collection.mutable.ArrayBuffer[Long] = users.map(_._1)

  override def getAllDocumentGroups: collection.mutable.ArrayBuffer[Long] = documentGroups.map(_._1)

  override def getAllPersons: collection.mutable.ArrayBuffer[Long] = persons.map(_._1)

  override def getAllRights: collection.mutable.ArrayBuffer[Long] = rights.map(_._1)

  //add
  override def addDocument(doc: Document): Long = {
    val id = iDsGenerator.generateDocumentID
    addPerson(doc.author)
    doc.getGroups.foreach(addGroup)
    documents.append((id, doc))
    id
  }

  override def addUser(user: User): Long = {
    val id = iDsGenerator.generateUserID
    users.append((id, user))
    rights.append((iDsGenerator.generateRightsID, user.rights))
    id
  }

  override def addGroup(group: Group): Long = {
    groups.find(_._2 == group) match {
      case Some((id, _)) => id
      case None =>
        val id = iDsGenerator.generateGroupID
        groups.append((id, group))
        addPerson(group.getLeader)
        group.getPersons.foreach(addPerson)
        id
    }
  }

  override def addPerson(person: Person): Long = {
    persons.find(_._2 == person) match {
      case Some((id, _)) => id
      case None =>
        val id = iDsGenerator.generatePersonID
        persons.append((id, person))
        person.getGroups.foreach(addGroup)
        id
    }
  }

  override def addDocumentGroup(documentGroup: DocumentGroup): Long = {
    val id = iDsGenerator.generateDocumentGroupID
    documentGroups.append((id, documentGroup))
    id
  }

  override def addRights(_rights: Rights): Long = {
    val id = iDsGenerator.generateRightsID
    rights.append((id, _rights))
    id
  }

  //get one
  override def getPersonByID(id: Long): Option[(String, collection.mutable.ArrayBuffer[Long])] = persons.find(_._1 == id).map(_._2).map(it => (it.name, it.getGroups.map(groupID)))

  override def getGroupByID(id: Long): Option[(String, Long, collection.mutable.ArrayBuffer[Long])] = groups.find(_._1 == id).map(it => (it._2.name, personID(it._2.getLeader), it._2.getPersons.map(personID)))

  override def getDocumentByID(id: Long): Option[(String, Long, collection.mutable.ArrayBuffer[Long], File, String)] = documents.find(_._1 == id).map(_._2).map(it => (it.name, personID(it.author), it.getGroups.map(groupID), it.file, it.externalID))

  override def getUserByID(id: Long): Option[(String, String, Long, String, String)] = users.find(_._1 == id).map(_._2).map(it => (it.name, it.accessLevel, rightsID(it.rights), it.login, it.pass))

  override def getDocumentGroupByID(id: Long): Option[(String, collection.mutable.ArrayBuffer[Long])] = documentGroups.find(_._1 == id).map(_._2).map(it => (it.name, it.getDocuments.map(documentID)))

  override def getRightsByID(id: Long): Option[(collection.mutable.ArrayBuffer[Long], collection.mutable.ArrayBuffer[Long])] = rights.find(_._1 == id).map(_._2).map(it => (it.getDocuments.map(documentID), it.getDocumentGroups.map(documentGroupID)))


  //util methods
  private def personID(person: Person): Long = {
    persons.find(_._2 == person) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No person with such id")
    }
  }
  private def groupID(group: Group): Long = {
    groups.find(_._2 == group) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No group with such id")
    }
  }
  private def documentID(document: Document): Long = {
    documents.find(_._2 == document) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No document with such id")
    }
  }
  private def documentGroupID(documentGroup: DocumentGroup): Long = {
    documentGroups.find(_._2 == documentGroup) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No document group with such id")
    }
  }
  private def rightsID(right: Rights): Long = {
    rights.find(_._2 == right) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No right with such id")
    }
  }
  private def userID(user: User): Long = {
    users.find(_._2 == user) match {
      case Some((id, _)) => id
      case _ => throw new RuntimeException("No user with such id")
    }
  }
}
