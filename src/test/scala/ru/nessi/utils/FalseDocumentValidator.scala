package ru.nessi.utils

import ru.nessi.business.Document
import ru.nessi.cases.DocumentValidator

class FalseDocumentValidator extends DocumentValidator{
  override def validate(document: Document): Boolean = false
}
