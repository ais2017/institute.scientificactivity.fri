package ru.nessi.utils

import java.io.File

import ru.nessi.business._

trait SpecHelper {

  val author = new Person("author")
  val person = new Person("person")
  val leader = new Person("leader")
  val group = new Group("group")
  val document = new Document("documentName", author)
  val documentFile = new File(getClass.getClassLoader.getResource("Document.txt").toURI)
  val documentGroup = new DocumentGroup("document group name")
  val user = new User("user", "USER", new Rights, "login", "pass")


}
