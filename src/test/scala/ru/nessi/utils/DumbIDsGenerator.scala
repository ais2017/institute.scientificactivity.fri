package ru.nessi.utils

import ru.nessi.storage.IDsGenerator

class DumbIDsGenerator extends IDsGenerator{

  var documentID: Long = -1
  var documentGroupID: Long = -1
  var groupID: Long = -1
  var personID: Long = -1
  var userID: Long = -1
  var rightsID: Long = -1

  def clear() = {
    documentGroupID = -1
    documentID = -1
    groupID = -1
    personID = -1
    userID = -1
    rightsID = -1
  }

  override def generateDocumentID: Long = {
    documentID += 1
    documentID
  }

  override def generateGroupID: Long = {
    groupID += 1
    groupID
  }

  override def generateDocumentGroupID: Long = {
    documentGroupID += 1
    documentGroupID
  }

  override def generatePersonID: Long = {
    personID += 1
    personID
  }

  override def generateUserID: Long = {
    userID += 1
    userID
  }

  override def generateRightsID: Long = {
    rightsID += 1
    rightsID
  }
}
